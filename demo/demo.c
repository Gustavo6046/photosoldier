#include <stdio.h>
#include <libpng/png.h>
#include <assert.h>

#include "commons.h"
#include "vector3.h"
#include "bitmap.h"
#include "scene.h"
#include "geometry.h"



char* version = "2019w33r1";
char* url = "https://gitlab.com/Gustavo6046/photosoldier";

Scene_t scene = {
    1,
    11,
    { 0.1, 0.15, 0.3 },
    NULL
};


int main() {
    int status = 0;

    printf("        photosoldier demo\n\n");
    printf("  written by: Gustavo Ramos Rehermann\n");
    printf("last version: %s\n\n", version);

    printf("Website:      %s\n\n", url);

    //=== 1. Initialize scene ===
    
    //     a) a simple sphere

    /*
    SphereData_t sp_data = {
        { 3.5, 2, 0 },   // center
        1.5              // radius
    };

    Material_t sp_mat = {
        { 0.01, 0.02, 0 }, // emmission
        { 0.2, 0.8, 0 },   // diffuse
        20000              // reflectivity
    };

    SceneObject_t* sp_obj = s_addObject(&scene, &G_dist_sphere, &sp_data, &sp_mat);
    */

    //    b) a box for a floor
    BoxData_t fl_data = {
        { 0, -1.5, 0 },  // center
        { 9, 0.5, 9 }  // axial radii
    };

    Material_t fl_mat = {
        { 0, 0, 0 },        // emission
        { 0.7, 0.65, 0.4 }, // diffuse
        1000
    };

    SceneObject_t* fl_obj = s_addObject(&scene, &G_dist_box, &fl_data, &fl_mat);

    //    c) a big sphere for extra light
    SphereData_t lt_data = {
        { 6, 7, 0 },
        3
    };

    Material_t lt_mat = {
        { 0.9, 0.775, 0.5 },
        { 0, 0, 0 },
        0
    };

    SceneObject_t* lt_obj = s_addObject(&scene, &G_dist_sphere, &lt_data, &lt_mat);

    //    d) a box on the floor
    BoxData_t bx_data = {
        { 4.25, 0.2, 1.5 },
        { 1, 0.5, 1.25 }
    };

    Material_t bx_mat = {
        { 0, 0, 0 },
        { 1, 0.7, 0.15 },     
        30000
    };

    SceneObject_t* bx_obj = s_addObject(&scene, &G_dist_box, &bx_data, &bx_mat);


    //=== 2. Define a camera and a bitmap, and raymarch! ===
    Camera_t cam = {
        { -1, 1, 0 },
        3.14159 * 95 / 180.,
        { 3.14159 * -15 / 180., 3.14159 * 8 / 180., 3.14159 * 3 / 180. }
    };

    Bitmap_t* bitmap = b_alloc(1366, 768);

    b_raymarchIntoBitmap(&scene, &cam, bitmap);

    //=== 3. Export the bitmap ===
    FILE* fp = fopen("demo_render.png", "w");

    if (fp == 0) {
        status = 1;
        goto clearUp;
    }

    b_writePNG(bitmap, fp);

clearUp:
    //=== 4. Deallocate everything ===
    b_done(bitmap);
    s_allDone(&scene, 0);

    return status;
}