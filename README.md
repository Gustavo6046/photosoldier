# photosoldier

photosoldier is a C project that attempts to simplify raymarching, providing
a concise but simple library for such endeavor, and also a demo program.

This project was written completely in C, mostly for the challenge of writing
a raymarcher.