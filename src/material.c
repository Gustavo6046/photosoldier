#include <stdio.h>

#include "commons.h"
#include "material.h"
#include "vector3.h"
#include "printcolor.h"



void mt_contribute(Color_t* target, Material_t* material, float importance) {
    /*
    printf("(input) ");
    printColor(target);
    */

    if (v_sqSize(&material->diffuse) > 0) {
        if (material->reflectivity < 65535) {

            Color_t* diffusion = &material->diffuse;
            Color_t* diffused = v_multiply(diffusion, target);
            Color_t* difference = v_sub(diffused, target);

            /*
            printf("(diffuse) ");
            printColor(&material->diffuse);
            printf("(full diffusion) ");
            printColor(diffused);
            printf("(full diffusion effect) ");
            printColor(difference);
            */

            v_addToScaled(target, difference, importance * (1.f - (float)(material->reflectivity) / 65535));
            
            v_done(difference);
            v_done(diffused);
        }
    }

    else {
        target->x = 0;
        target->y = 0;
        target->z = 0;
    }

    v_addToScaled(target, &material->emission, importance);
    
    /*
    printf("(emission) ");
    printColor(&material->emission);

    printf("(importance: %.2f%% | reflectivity: %.2f%% | diffusion effect: %.2f%%)\n",
        importance * 100, 
        (float) (material->reflectivity) * 100 / 65536,
        (importance * (1.f - (float)(material->reflectivity) / 65535)) * 100
    );
    
    printf("(post) ");
    printColor(target);
    printf("\n");
    */
}