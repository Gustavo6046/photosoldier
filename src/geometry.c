#include <math.h>
#include <stdlib.h>

#include "commons.h"
#include "vector3.h"
#include "geometry.h"



float G_dist_sphere(SceneObject_t* me, Vec3_t* to) {
    SphereData_t* data = ((SphereData_t*) me->data);

    Vec3_t* offs = v_sub(to, &(data->pos));
    float len = v_size(offs) - data->rad;

    v_done(offs);
    return len > 0 ? len : 0;
}

float G_dist_box(SceneObject_t* me, Vec3_t* to) {
    BoxData_t* data = ((BoxData_t*) me->data);

    Vec3_t* offs = v_sub(to, &(data->pos));

    if (offs->x < 0) offs->x *= -1;
    if (offs->y < 0) offs->y *= -1;
    if (offs->z < 0) offs->z *= -1;

    float dx = offs->x - data->sizes.x;
    float dy = offs->y - data->sizes.y;
    float dz = offs->z - data->sizes.z;

    if (dx < 0) dx = 0;
    if (dy < 0) dy = 0;
    if (dz < 0) dz = 0;

    float len = sqrtf(dx * dx + dy * dy + dz * dz);

    v_done(offs);
    return len > 0 ? len : 0;
}


void G_init_sphere(SceneObject_t* me, Vec3_t* pos, float rad) {
    SphereData_t* data = malloc(sizeof(SphereData_t));
    
    data->pos = *pos;
    data->rad = rad;
    
    me->data = &data;
}

void G_init_box(SceneObject_t* me, Vec3_t* pos, Vec3_t* box) {
    BoxData_t* data = malloc(sizeof(BoxData_t));
    
    data->pos = *pos;
    data->sizes = *box;
    
    me->data = &data;
}