#include <stdio.h>

#include "marcher.h"
#include "commons.h"
#include "vector3.h"
#include "ray.h"
#include "material.h"
#include "geometry.h"


#define EPSILON 0.001F
#define MIN_DIST 0.01F



unsigned short maxBounces = 3;
float importanceFadeDist = 10;


void m_rayMarch(Scene_t* scene, Ray_t* ray, MarchResult_t* into) {
    float sDist;
    
    Vec3_t* cOffs;
    Vec3_t* pos;

    into->steps = 0;
    into->hit = 0;

    ray->currDist = 0;

    while (1) {
        pos = r_currentPos(ray);

        into->distance = ray->currDist;
        into->stop = pos;
        into->steps++;

        if (ray->currDist > scene->farDist) {
            into->hit = 0;
            into->hitDist = 0;

            break;
        }

        sDist = r_sceneDistance(ray, scene);

        if (sDist < MIN_DIST && ray->closest) {
            into->hit = ray->closest;
            into->hitDist = sDist;

            break;
        }

        else
            r_advance(ray, sDist);
    }
}


Ray_t* m_bounceRay(Scene_t* scene, Ray_t* ray, MarchResult_t* thisRayHit) {
    if (thisRayHit->hit == 0) m_rayMarch(scene, ray, thisRayHit);
    if (thisRayHit->hit == 0) return 0;

    Vec3_t* normal = m_approxNormal(scene, thisRayHit);

    Vec3_t* direction = v_subScaled(ray->toward, normal, v_dot(ray->toward, normal) * 2);
    v_normalizeAt(direction);

    Ray_t* newRay = r_fromToward(thisRayHit->stop, direction);
    v_addToScaled(newRay->origin, normal, MIN_DIST * 1.1);

    v_done(normal);
    return newRay;
}


Vec3_t* m_approxNormal(Scene_t* scene, MarchResult_t* where) {
    Vec3_t* normal = v_new();
    Vec3_t* at = where->stop;
    Vec3_t* at_xp = v_addScaled(at, &vx, EPSILON);
    Vec3_t* at_yp = v_addScaled(at, &vy, EPSILON);
    Vec3_t* at_zp = v_addScaled(at, &vz, EPSILON);
    Vec3_t* at_xm = v_subScaled(at, &vx, EPSILON);
    Vec3_t* at_ym = v_subScaled(at, &vy, EPSILON);
    Vec3_t* at_zm = v_subScaled(at, &vz, EPSILON);

    float distX = (*where->hit->distanceFunc)(where->hit, at_xp) - (*where->hit->distanceFunc)(where->hit, at_xm);
    float distY = (*where->hit->distanceFunc)(where->hit, at_yp) - (*where->hit->distanceFunc)(where->hit, at_ym);
    float distZ = (*where->hit->distanceFunc)(where->hit, at_zp) - (*where->hit->distanceFunc)(where->hit, at_zm);

    // printf("ND %f %f %f\n", distX, distY, distZ);

    v_done(at_xp);
    v_done(at_yp);
    v_done(at_zp);

    v_done(at_xm);
    v_done(at_ym);
    v_done(at_zm);

    v_addToScaled(normal, &vx, distX);
    v_addToScaled(normal, &vy, distY);
    v_addToScaled(normal, &vz, distZ);
    v_normalizeAt(normal);

    return normal;
}


Vec3_t* m_approxNormalAt(Scene_t* scene, Vec3_t* where, SceneObject_t* from) {
    Vec3_t* normal = v_new();
    Vec3_t* at = where;
    Vec3_t* at_xp = v_addScaled(at, &vx, EPSILON);
    Vec3_t* at_yp = v_addScaled(at, &vy, EPSILON);
    Vec3_t* at_zp = v_addScaled(at, &vz, EPSILON);
    Vec3_t* at_xm = v_subScaled(at, &vx, EPSILON);
    Vec3_t* at_ym = v_subScaled(at, &vy, EPSILON);
    Vec3_t* at_zm = v_subScaled(at, &vz, EPSILON);

    float distX = (*from->distanceFunc)(from, at_xp) - (*from->distanceFunc)(from, at_xm);
    float distY = (*from->distanceFunc)(from, at_yp) - (*from->distanceFunc)(from, at_ym);
    float distZ = (*from->distanceFunc)(from, at_zp) - (*from->distanceFunc)(from, at_zm);

    // printf("ND %f %f %f\n", distX, distY, distZ);

    v_done(at_xp);
    v_done(at_yp);
    v_done(at_zp);

    v_done(at_xm);
    v_done(at_ym);
    v_done(at_zm);

    v_addToScaled(normal, &vx, distX);
    v_addToScaled(normal, &vy, distY);
    v_addToScaled(normal, &vz, distZ);
    v_normalizeAt(normal);

    return normal;
}


typedef struct Bounce_s {
    float importance;
    MarchResult_t res;
    float fog;
    Vec3_t toward;
} Bounce_t;


void m_applyDiffuse(Color_t* intoPixel, Scene_t* scene, Vec3_t* from, Vec3_t* viewDirection, SceneObject_t* hit, float importance) {
    float diffuseStrength = 1.F - (float) (hit->material.reflectivity) / 65536 * importance;
    SceneObject_t* light = scene->objects;
    MarchResult_t marchResult;
    Vec3_t* norm;
    Vec3_t* liteDir;
    float power;

    while (light) {
        if (light != hit && v_sqSize(&light->material.emission) > 0) {
            norm = m_approxNormalAt(scene, from, hit);
            liteDir = m_approxNormalAt(scene, from, light);
            v_neg(liteDir);

            power = v_dot(norm, viewDirection);

            if (power > 0) {
                power *= diffuseStrength;

                Vec3_t* actualFrom = v_duplicate(from);
                v_addToScaled(actualFrom, norm, MIN_DIST * 1.1);

                Ray_t* myRay = r_fromToward(actualFrom, liteDir);
                m_rayMarch(scene, myRay, &marchResult);

                v_done(actualFrom);

                printf("hit light: %i   |   hit self: = %i\n", marchResult.hit == light, marchResult.hit == hit);
                if (marchResult.hit == light) {
                    v_addToScaled(intoPixel, &hit->material.diffuse, power);
                }
            }

            v_done(norm);
            v_done(liteDir);
        }

        light = light->next;
    }
}


void m_fullRayMarch(Scene_t* scene, Ray_t* ray, Color_t* intoPixel) {
    float importance = 1.0;
    unsigned short bounces = 1;
    MarchResult_t postMarch;
    Ray_t* currRay = ray;
    Bounce_t* allBounces = malloc(sizeof(Bounce_t) * maxBounces);
    Bounce_t* bouncesStart = allBounces;
    Bounce_t* bouncesEnd = allBounces + maxBounces;
    Color_t* fogDiff;
    
    v_zero(intoPixel);

    while (importance > 0.025 && bounces <= maxBounces && allBounces < bouncesEnd) {
        m_rayMarch(scene, currRay, &postMarch);
        // printf("D %f  H %f\n", postMarch.distance, postMarch.hitDist);

        if (postMarch.distance > scene->farDist) {
            allBounces--;
            break;
        }

        importance -= postMarch.distance / importanceFadeDist;

        if (postMarch.hit == 0) {
            allBounces--;
            break;
        }

        allBounces->importance = importance;
        allBounces->res = postMarch;
        allBounces->fog = 0;
        allBounces->toward = *currRay->toward;

        // for debugging
        /*
        if (allBounces == bouncesStart && postMarch.hit != 0) {
            printf("(%f, %f, %f) HIT %p far %f\n", postMarch.stop->x, postMarch.stop->y, postMarch.stop->z, postMarch.hit, postMarch.hitDist);
        }
        */

        if (postMarch.distance >= scene->nearDist) {
            allBounces->fog = (postMarch.distance - scene->nearDist) / (scene->farDist - scene->nearDist);
        }

        importance /= 1.3;

        bounces++;

        if (importance > 0.025 && bounces <= maxBounces && allBounces < bouncesEnd) {
            allBounces++;
            currRay = m_bounceRay(scene, currRay, &postMarch);
        }
    }

    if (allBounces < bouncesStart) {
        *intoPixel = scene->fogColor;
    }

    else while (allBounces >= bouncesStart) {
        if (allBounces->res.hit != 0) {
            /*
            printf("bounce %i\n", allBounces - bouncesStart);
            printf("bouptr %p\n", allBounces);
            printf("hitptr %p\n", allBounces->res.hit);
            printf("id %i\n", allBounces->res.hit->id);
            */

            if (allBounces->res.hit->material.reflectivity < 65535) {
                // Diffuse lighting
                m_applyDiffuse(intoPixel, scene, allBounces->res.stop, &allBounces->toward, allBounces->res.hit, importance);
            }

            mt_contribute(intoPixel, &allBounces->res.hit->material, allBounces->importance);
        }

        else *intoPixel = scene->fogColor;

        if (allBounces->fog > 0) {
            fogDiff = v_sub(&scene->fogColor, intoPixel);
            v_addScaled(intoPixel, fogDiff, allBounces->fog);
            v_done(fogDiff);
        }

        allBounces--;
    }

    free(bouncesStart);
}