#ifndef VECTOR_HEADER
#define VECTOR_HEADER



extern Vec3_t vx;
extern Vec3_t vy;
extern Vec3_t vz;


void v_zero(Vec3_t* ptr);
Vec3_t* v_new();
Vec3_t* v_allocMany(size_t howMany);
Vec3_t* v_duplicate(Vec3_t* from);
void v_done(Vec3_t* which);

Vec3_t* v_add(Vec3_t* vec1, Vec3_t* vec2);
Vec3_t* v_sub(Vec3_t* vec1, Vec3_t* vec2);
Vec3_t* v_addScaled(Vec3_t* vec1, Vec3_t* vec2, float weight);
Vec3_t* v_subScaled(Vec3_t* vec1, Vec3_t* vec2, float weight);
void v_neg(Vec3_t* vec);
Vec3_t* v_scale(Vec3_t* vec, float scale);
Vec3_t* v_multiply(Vec3_t* vec1, Vec3_t* vec2);
Vec3_t* v_divide(Vec3_t* vec1, float by);
Vec3_t* v_normalize(Vec3_t* vec);

float v_size(Vec3_t* vec);
float v_sqSize(Vec3_t* vec);
float v_dot(Vec3_t* vec1, Vec3_t* vec2);

void v_addTo(Vec3_t* vec1, Vec3_t* vec2);
void v_subTo(Vec3_t* vec1, Vec3_t* vec2);
void v_addToScaled(Vec3_t* vec1, Vec3_t* vec2, float weight);
void v_subToScaled(Vec3_t* vec1, Vec3_t* vec2, float weight);
void v_scaleTo(Vec3_t* vec, float scale);
void v_multiplyTo(Vec3_t* vec1, Vec3_t* vec2);
void v_divideTo(Vec3_t* vec1, float by);
void v_normalizeAt(Vec3_t* vec);

void v_rotate(Vec3_t* vec, Rot_t* rot);


#endif