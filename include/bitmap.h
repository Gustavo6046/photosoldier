#ifndef BITMAP_HEADER
#define BITMAP_HEADER

#include <stdio.h>

#include "commons.h"



int b_writePNG(Bitmap_t* bitmap, FILE* fp);
Color_t* b_at(Bitmap_t* bitmap, size_t x, size_t y);
Bitmap_t* b_alloc(size_t width, size_t height);
void b_done(Bitmap_t* toFree);
void b_raymarchIntoBitmap(Scene_t* scene, Camera_t* cam, Bitmap_t* bitmap);



#endif