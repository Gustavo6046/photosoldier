#ifndef MARCHER_HEADER
#define MARCHER_HEADER

#include "commons.h"



extern unsigned short maxBounces;

typedef struct MarchResult_s {
    Vec3_t* stop;
    SceneObject_t* hit;
    float distance;
    float hitDist;
    unsigned short steps;
} MarchResult_t;

void m_rayMarch(Scene_t* scene, Ray_t* ray, MarchResult_t* into);
void m_applyDiffuse(Color_t* intoPixel, Scene_t* scene, Vec3_t* from, Vec3_t* viewDirection, SceneObject_t* hit, float importance);
Vec3_t* m_approxNormal(Scene_t* scene, MarchResult_t* where);
Ray_t* m_bounceRay(Scene_t* scene, Ray_t* ray, MarchResult_t* thisRayHit);
void m_fullRayMarch(Scene_t* scene, Ray_t* ray, Color_t* intoPixel);



#endif